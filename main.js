dayjs.extend(dayjs_plugin_customParseFormat);

/**
 * @param {string} birthday 
 */
function getAge(birthday) {
  if (birthday && birthday.split(' ').length == 3) {
    const dateNow = dayjs(Date.now());
    const birthdayDate = dayjs(birthday, 'D MMMM YYYY');
    const ageInYears = dateNow.diff(birthdayDate, 'year');

    let age = ageInYears;
    if (ageInYears > 1) {
      age += ` years old`;
    } else {
      age = ` year old`;
    }
    return age;
  }
  return null;
}

function addClass(el, className) {
  if (el.classList)
    el.classList.add(className);
  else
    el.className += ' ' + className;
}

function showAge() {
  const contactDetailsEl = document.querySelector('.NVFbjd.LAORIe.Hdzgyf');
  // Since the fields have nothing to uniquely identify them, find the birthday field using the birthday icon.
  const birthdayIcon = contactDetailsEl.querySelector('path[d = "M12 6c1.11 0 2-.9 2-2 0-.38-.1-.73-.29-1.03L12 0l-1.71 2.97c-.19.3-.29.65-.29 1.03 0 1.1.9 2 2 2zm6 3h-5V7h-2v2H6c-1.66 0-3 1.34-3 3v9c0 .55.45 1 1 1h16c.55 0 1-.45 1-1v-9c0-1.66-1.34-3-3-3zm1 11H5v-3c.9-.01 1.76-.37 2.4-1.01l1.09-1.07 1.07 1.07c1.31 1.31 3.59 1.3 4.89 0l1.08-1.07 1.07 1.07c.64.64 1.5 1 2.4 1.01v3zm0-4.5c-.51-.01-.99-.2-1.35-.57l-2.13-2.13-2.14 2.13c-.74.74-2.03.74-2.77 0L8.48 12.8l-2.14 2.13c-.35.36-.83.56-1.34.57V12c0-.55.45-1 1-1h12c.55 0 1 .45 1 1v3.5z"]');
  if (birthdayIcon) {
    // Then go up a level to the field.
    const fieldEl = birthdayIcon.closest('.E49qB');
    // Finally, get the actual birthday element.
    const birthdayEl = fieldEl.querySelector('.jBcx8c.O1gPMb > span:nth-child(1)');

    if (birthdayEl) {
      let birthday = birthdayEl.innerText;
      const age = getAge(birthday);
      const birthdayFieldValue = fieldEl.querySelector('.jBcx8c.O1gPMb');
      const ageEl = document.createElement('span');
      addClass(ageEl, 'SiTyAd');
      ageEl.innerText += `  • ${age}`;
      birthdayFieldValue.appendChild(ageEl);
    }
  }

}

const observer = new MutationObserver((mutations) => {
  for (const mutation of mutations) {
    if (mutation.addedNodes.length > 0) {
      const [addedNode] = mutation.addedNodes;
      // If a contact has been opened and the birthday field has loaded
      if (addedNode.className === 'NVFbjd LAORIe Hdzgyf') {
        showAge();
      }
    }
  }
});
const target = document.querySelector('.MCcOAc');
observer.observe(target, { childList: true, subtree: true });
